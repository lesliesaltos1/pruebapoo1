package conn.interfa;

import java.util.List;

import conn.entidades.Cursos;
import conn.entidades.Cursos_estudiante;


public interface Cursos_estudianteDAO {
	
	List<Cursos_estudiante> getCursos_estudiante();
	public void ingresar(Cursos_estudiante cur);
	public void actualizar(Cursos_estudiante cur);
	public void eliminar(int id);

}
