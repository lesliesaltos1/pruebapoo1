package com.cursoDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conn.entidades.Cursos_estudiante;
import conn.entidades.Estudiante;
import conn.interfa.Cursos_estudianteDAO;
import conn.util.Util;

public class Cursos_estudianteDAOImpl implements Cursos_estudianteDAO{

	@Override
	public List<Cursos_estudiante> getCursos_estudiante() {
		List<Cursos_estudiante> estudiante = new ArrayList<>(); 
		Connection con; 
	        
		try 
	       {
	           con = DriverManager.getConnection(Util.URL, Util.USUARIO, Util.CLAVE);
	           
	           
	      
	           String sql ="SELECT id, cursos_id, estudiantes_id\r\n" + 
	           		"	FROM public.cursos_estudiantes;";
	           
	           PreparedStatement stm = con.prepareStatement(sql);
	           ResultSet rs = stm.executeQuery();
	           while(rs.next()){
	        	   Cursos_estudiante estudiant = new Cursos_estudiante
	        			   (rs.getInt(1), rs.getInt(2), rs.getInt(3));
	        	   estudiante.add(estudiant) ;        
	            }      
	           
	           
	       }   
	         catch (SQLException e) {
	        	 e.printStackTrace();
	         }
			return estudiante;
		
	}

	@Override
	public void ingresar(Cursos_estudiante cur) {
		String query ="INSERT INTO public.cursos_estudiantes(id, cursos_id, estudiantes_id)VALUES (?, ?, ?)";
		Connection conn;
		try
	       {
		   conn = DriverManager.getConnection(Util.URL, Util.USUARIO, Util.CLAVE);
		   		 

			PreparedStatement stm = conn.prepareStatement(query);  
			stm.setInt(1, cur.getId());
			stm.setInt(2, cur.getCursos_id());
			stm.setInt(3, cur.getEsudiantes_id());

			
			stm.execute();
			   
		   }
		   	   
	   catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

	@Override
	public void actualizar(Cursos_estudiante cur) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void eliminar(int id) {
		// TODO Auto-generated method stub
		
	}

}
